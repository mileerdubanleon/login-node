const express = require('express');
const { engine } = require('express-handlebars');
const myconnection = require('express-myconnection');
const mysql = require('mysql');
const session = require('express-session');
const bodyParser = require('body-parser');

const loginRoutes = require('./routes/login');

const app = express(); // Crea una instancia de la aplicación Express.
app.set('port', 4000); // Configura el puerto en el que la aplicación se va a visualizar.

app.set('views', __dirname + '/views'); // Establece la ubicación de las vistas (plantillas) para la aplicación.

app.engine('.hbs', engine({
    extname: '.hbs',
})); // Configura el motor de plantillas Handlebars para renderizar archivos .hbs.

app.set('view engine', 'hbs');  // Establece el motor de plantillas predeterminado como Handlebars.

app.use(bodyParser.urlencoded({
    extended: true
})); // Agrega un middleware para analizar datos enviados desde formularios HTML.

app.use(bodyParser.json()); // Agrega un middleware para analizar datos JSON en solicitudes.

app.use(myconnection(mysql, {
    host: 'localhost',
    user: "root",
    password: '',
    port: 3306,
    database: 'nodelogin'
})); // Agrega un middleware para establecer una conexión a la base de datos MySQL

app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));  // Agrega un middleware para administrar sesiones, con una clave secreta y opciones.

app.listen(app.get('port'), () => {
    console.log('listening on port', app.get('port'));
});  // Inicia el servidor y comienza a escuchar en el puerto especificado.

app.use('/', loginRoutes);

app.get('/', (req, res)=> {
    if(req.session.loggedin == true) {
        res.render('home', {name: req.session.name });
    } else {
        res.redirect('/login');
    }
});  // Manejador de ruta para la URL raíz ('/'). Renderiza la vista 'home'.